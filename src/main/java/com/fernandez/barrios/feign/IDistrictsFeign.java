package com.fernandez.barrios.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.fernandez.barrios.dto.DistrictsDTO;


@FeignClient(name="ms-districts")
public interface IDistrictsFeign {

	@GetMapping(value = "/")
	public ResponseEntity<List<DistrictsDTO>> getDistricts(); 
	
	@GetMapping(value = "/{district}")
    public ResponseEntity<DistrictsDTO> getDistrictsById(@PathVariable("district") String district);
}
