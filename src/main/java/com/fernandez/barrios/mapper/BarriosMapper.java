package com.fernandez.barrios.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.barrios.dto.BarriosDTO;
import com.fernandez.barrios.dto.DistrictsDTO;
import com.fernandez.barrios.feign.IDistrictsFeign;
import com.fernandez.barrios.model.Barrios;

@Component
public class BarriosMapper {
	
	@Autowired
	private IDistrictsFeign districtsFeign;
	
	public List<BarriosDTO> mapToBarriosListDTO(List<Barrios> barriosList) {
				
		List<BarriosDTO> barriosDTOList = new ArrayList<BarriosDTO>();
		
		for(Barrios barrios : barriosList) {
			barriosDTOList.add(mapToBarriosDTO(barrios));
		}
	
		return barriosDTOList;
	}
	
	
	public BarriosDTO mapToBarriosDTO(Barrios barrios) {
		BarriosDTO barriosDTO = BarriosDTO.builder()
				.idBarrio(barrios.getIdBarrio())
				.codigoBarrio(barrios.getCodigoBarrio())
				.districtsDTO(findDistrictById(barrios.getCodigoDistrito()))
				.nombreDeBarrio(barrios.getNombreDeBarrio())
				.nombreAcentuadoBarrio(barrios.getNombreAcentuadoBarrio())
				.superficie(barrios.getSuperficie())
				.perimetros(barrios.getPerimetros())
				.build();
		return barriosDTO;
	}

	private DistrictsDTO findDistrictById(String codigoDistrito) {
		DistrictsDTO districtsDTO = districtsFeign.getDistrictsById(codigoDistrito).getBody();
		return districtsDTO;
	}

}
