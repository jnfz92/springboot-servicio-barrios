package com.fernandez.barrios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;
@Builder
@Data
public class BarriosDTO {

	@JsonProperty("id")
	private Long idBarrio;
	
	private String codigoBarrio;
	
	private DistrictsDTO districtsDTO;

	private String nombreDeBarrio;
	
	private String nombreAcentuadoBarrio;
	
	private String superficie;
	
	private String perimetros;

	
	
}
