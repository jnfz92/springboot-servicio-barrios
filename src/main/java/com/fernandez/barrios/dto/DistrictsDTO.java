package com.fernandez.barrios.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DistrictsDTO {
	
    @JsonProperty("id")
	private Long idDistrict;
	private String codigoDistrito;
	private String nombreDistrito;
	private String nombreAcentuadoDelDistrito;
	private Integer port;

}
