package com.fernandez.barrios.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.fernandez.barrios.dto.BarriosDTO;

public interface IBarriosController {

	ResponseEntity<List<BarriosDTO>> getBarrios();

	ResponseEntity<BarriosDTO> getBarrioById(String barrioId) throws Exception;

	ResponseEntity<?> getConfiguration() throws Exception;

}
