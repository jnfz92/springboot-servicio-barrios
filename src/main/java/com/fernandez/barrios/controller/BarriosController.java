package com.fernandez.barrios.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.barrios.dto.BarriosDTO;
import com.fernandez.barrios.services.IBarriosServices;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RefreshScope
@RestController
public class BarriosController implements IBarriosController{

	@Autowired
	private IBarriosServices barrios;
	
	@Value("${spring.application.name}")
	private String texto;
	
	@Value("${my.properties.some_mandatory_property}")
	private String property;
	
	@Autowired
	private Environment env;
	
	@GetMapping(value = "/")
	@Override
    public ResponseEntity<List<BarriosDTO>> getBarrios() {
        return new ResponseEntity<List<BarriosDTO>>(barrios.findAll(), HttpStatus.OK);
    }
	
	@HystrixCommand(fallbackMethod="metodoAlternativo")
	@GetMapping(value = "/{barrio}")
	@Override
    public ResponseEntity<BarriosDTO> getBarrioById( @PathVariable("barrio") String barrio) throws Exception {
		BarriosDTO barriosDTO = barrios.findById(Long.valueOf(barrio));
        return new ResponseEntity<BarriosDTO>(barriosDTO, HttpStatus.OK);
    }
	
	public ResponseEntity<BarriosDTO> metodoAlternativo(String barrio){
		BarriosDTO barriosDTO = BarriosDTO.builder().build();
        return new ResponseEntity<BarriosDTO>(barriosDTO, HttpStatus.OK);
	}
	
	@GetMapping(value = "/config")
	@Override
    public ResponseEntity<?> getConfiguration() throws Exception {
		Map<String,String> json = new HashMap<>();
		json.put("texto", texto);
		json.put("property", property);
		if(env.getActiveProfiles().length>0 && env.getActiveProfiles()[0].equals("dev")) {
			json.put("configuracion.autor.nombre", env.getProperty("configuracion.autor.nombre"));
		}
		return new ResponseEntity<Map<String,String>>(json, HttpStatus.OK);

	}
	
	
	
}
