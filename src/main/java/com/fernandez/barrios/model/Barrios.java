package com.fernandez.barrios.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "barrios")
public class Barrios {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idBarrio;

	@Column(name = "codigo_barrio")
	private String codigoBarrio;

	@Column(name = "codigo_distrito")
	private String codigoDistrito;

	@Column(name = "nombre_barrio")
	private String nombreDeBarrio;

	@Column(name = "nombre_acentuado_barrio")
	private String nombreAcentuadoBarrio;

	@Column(name = "superficie")
	private String superficie;

	@Column(name = "perimetro")
	private String perimetros;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoBarrio == null) ? 0 : codigoBarrio.hashCode());
		result = prime * result + ((codigoDistrito == null) ? 0 : codigoDistrito.hashCode());
		result = prime * result + ((idBarrio == null) ? 0 : idBarrio.hashCode());
		result = prime * result + ((nombreAcentuadoBarrio == null) ? 0 : nombreAcentuadoBarrio.hashCode());
		result = prime * result + ((nombreDeBarrio == null) ? 0 : nombreDeBarrio.hashCode());
		result = prime * result + ((perimetros == null) ? 0 : perimetros.hashCode());
		result = prime * result + ((superficie == null) ? 0 : superficie.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Barrios other = (Barrios) obj;
		if (codigoBarrio == null) {
			if (other.codigoBarrio != null)
				return false;
		} else if (!codigoBarrio.equals(other.codigoBarrio))
			return false;
		if (codigoDistrito == null) {
			if (other.codigoDistrito != null)
				return false;
		} else if (!codigoDistrito.equals(other.codigoDistrito))
			return false;
		if (idBarrio == null) {
			if (other.idBarrio != null)
				return false;
		} else if (!idBarrio.equals(other.idBarrio))
			return false;
		if (nombreAcentuadoBarrio == null) {
			if (other.nombreAcentuadoBarrio != null)
				return false;
		} else if (!nombreAcentuadoBarrio.equals(other.nombreAcentuadoBarrio))
			return false;
		if (nombreDeBarrio == null) {
			if (other.nombreDeBarrio != null)
				return false;
		} else if (!nombreDeBarrio.equals(other.nombreDeBarrio))
			return false;
		if (perimetros == null) {
			if (other.perimetros != null)
				return false;
		} else if (!perimetros.equals(other.perimetros))
			return false;
		if (superficie == null) {
			if (other.superficie != null)
				return false;
		} else if (!superficie.equals(other.superficie))
			return false;
		return true;
	}

	public Long getIdBarrio() {
		return idBarrio;
	}

	public void setIdBarrio(Long idBarrio) {
		this.idBarrio = idBarrio;
	}

	public String getCodigoBarrio() {
		return codigoBarrio;
	}

	public void setCodigoBarrio(String codigoBarrio) {
		this.codigoBarrio = codigoBarrio;
	}

	public String getCodigoDistrito() {
		return codigoDistrito;
	}

	public void setCodigoDistrito(String codigoDistrito) {
		this.codigoDistrito = codigoDistrito;
	}

	public String getNombreDeBarrio() {
		return nombreDeBarrio;
	}

	public void setNombreDeBarrio(String nombreDeBarrio) {
		this.nombreDeBarrio = nombreDeBarrio;
	}

	public String getNombreAcentuadoBarrio() {
		return nombreAcentuadoBarrio;
	}

	public void setNombreAcentuadoBarrio(String nombreAcentuadoBarrio) {
		this.nombreAcentuadoBarrio = nombreAcentuadoBarrio;
	}

	public String getSuperficie() {
		return superficie;
	}

	public void setSuperficie(String superficie) {
		this.superficie = superficie;
	}

	public String getPerimetros() {
		return perimetros;
	}

	public void setPerimetros(String perimetros) {
		this.perimetros = perimetros;
	}

	@Override
	public String toString() {
		return "Barrios [idBarrio=" + idBarrio + ", codigoBarrio=" + codigoBarrio + ", codigoDistrito=" + codigoDistrito
				+ ", nombreDeBarrio=" + nombreDeBarrio + ", nombreAcentuadoBarrio=" + nombreAcentuadoBarrio
				+ ", superficie=" + superficie + ", perimetros=" + perimetros + "]";
	}

}
