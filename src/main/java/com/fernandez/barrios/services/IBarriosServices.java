package com.fernandez.barrios.services;

import java.util.List;

import com.fernandez.barrios.dto.BarriosDTO;

public interface IBarriosServices {

	List<BarriosDTO> findAll();

	BarriosDTO findById(Long barriosId);

}
