package com.fernandez.barrios.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fernandez.barrios.dto.BarriosDTO;
import com.fernandez.barrios.mapper.BarriosMapper;
import com.fernandez.barrios.model.Barrios;
import com.fernandez.barrios.repository.BarriosRepository;
import com.fernandez.barrios.utils.BarriosUtils;

@Service
public class BarriosServices implements IBarriosServices {

	@Autowired
	private BarriosRepository barriosRepository;
	
	@Autowired
	private BarriosMapper barriosMapper;
	
	
	@Override
	public List<BarriosDTO> findAll() {
		List<BarriosDTO> barriosDTOList = new ArrayList<BarriosDTO>();
		List<Barrios> barriosList = barriosRepository.findAll();
		if (!BarriosUtils.isNullOrEmpty(barriosList)) {
			barriosDTOList = barriosMapper.mapToBarriosListDTO(barriosList);
		}
		return barriosDTOList;	
	}
	
	@Override
	public BarriosDTO findById(Long barriosId) {
		return barriosMapper.mapToBarriosDTO(barriosRepository.findById(barriosId).get());
	}
}
