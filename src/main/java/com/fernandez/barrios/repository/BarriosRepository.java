package com.fernandez.barrios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fernandez.barrios.model.Barrios;

public interface BarriosRepository  extends JpaRepository<Barrios, Long>{

}
