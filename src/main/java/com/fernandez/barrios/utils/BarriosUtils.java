package com.fernandez.barrios.utils;

import java.util.Collection;

import lombok.experimental.UtilityClass;



@UtilityClass
public class BarriosUtils {

	public <T> boolean isNullOrEmpty(Collection<T> list) {
		return list == null || list.isEmpty();
	}

}
